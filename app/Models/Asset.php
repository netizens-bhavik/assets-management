<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory, HasUuids;
    protected $fillable = [
        'id',
        'owner',
        'phone_number',
        'establishment_date',
        'floor_number',
        'assets_location',
        'asset_location_type',
        'asset_categories',
        'asset_name',
        'date_of_purchase',
        'value_of_purchase',
        'purchase_type',
        'asset_condition',
    ];
    public function setEstablishmentDateAttribute($value)
    {
        $this->attributes['establishment_date'] = date('Y-m-d', strtotime($value));
    }

    // Mutator for date_of_purchase
    public function setDateOfPurchaseAttribute($value)
    {
        $this->attributes['date_of_purchase'] = date('Y-m-d', strtotime($value));
    }

    public function getEstablishmentDateAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    // Accessor for date_of_purchase
    public function getDateOfPurchaseAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function setAssetCategoriesAttribute($value)
    {
        $this->attributes['asset_categories'] = strtolower($value);
    }

    public function setAssetNameAttribute($value)
    {
        $this->attributes['asset_name'] = strtolower($value);
    }

    public function setPurchaseTypeAttribute($value)
    {
        $this->attributes['purchase_type'] = strtolower($value);
    }

    public function setAssetConditionAttribute($value)
    {
        $this->attributes['asset_condition'] = strtolower($value);
    }
    public function setAssetLocationTypeAttribute($value)
    {
        $this->attributes['asset_location_type'] = strtolower($value);
    }

    // Accessors
    public function getAssetCategoriesAttribute($value)
    {
        return str_replace(' ', '_', strtolower($value));
    }

    public function getAssetNameAttribute($value)
    {
        return strtolower($value);
    }

    public function getPurchaseTypeAttribute($value)
    {
        return strtolower($value);
    }

    public function getAssetConditionAttribute($value)
    {
        return str_replace(' ', '_', strtolower($value));
    }

    public function getAssetLocationTypeAttribute($value)
    {
        return str_replace(' ', '_', strtolower($value));
    }
}
