<?php

namespace App\Filament\Exports;

use App\Models\Asset;
use Filament\Actions\Exports\ExportColumn;
use Filament\Actions\Exports\Exporter;
use Filament\Actions\Exports\Models\Export;

class AssetExporter extends Exporter
{
    protected static ?string $model = Asset::class;

    public static function getColumns(): array
    {
        return [
            ExportColumn::make('id')
                ->label('ID'),
            ExportColumn::make('owner')->label('NAME OF THE ASSET OWNER'),
            ExportColumn::make('phone_number')->label('MOBILE NUMBER'),
            ExportColumn::make('establishment_date')->label('ESTABLISHMENT DATE(BRANCH/HO)'),
            ExportColumn::make('floor_number')->label('FLOOR NO.'),
            ExportColumn::make('assets_location')->label('LOCATION OF ASSET'),
            ExportColumn::make('asset_location_type')->label('SHARED DESK/CABIN'),
            ExportColumn::make('asset_categories')->label('CATEGORY OF ASSET '),
            ExportColumn::make('asset_name')->label('ASSET NAME'),
            ExportColumn::make('date_of_purchase')->label('DATE OF PURCHASE'),
            ExportColumn::make('value_of_purchase')->label('VALUE OF PURCHASE'),
            ExportColumn::make('purchase_type')->label('NEWLY PURCHASE/SHIFTED'),
            ExportColumn::make('asset_condition')->label('WORKING CONDITION'),
        ];
    }

    public static function getCompletedNotificationBody(Export $export): string
    {
        $body = 'Your asset export has completed and ' . number_format($export->successful_rows) . ' ' . str('row')->plural($export->successful_rows) . ' exported.';

        if ($failedRowsCount = $export->getFailedRowsCount()) {
            $body .= ' ' . number_format($failedRowsCount) . ' ' . str('row')->plural($failedRowsCount) . ' failed to export.';
        }

        return $body;
    }
}
