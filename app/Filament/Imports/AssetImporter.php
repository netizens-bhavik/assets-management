<?php

namespace App\Filament\Imports;

use App\Models\Asset;
use Filament\Actions\Imports\ImportColumn;
use Filament\Actions\Imports\Importer;
use Filament\Actions\Imports\Models\Import;

class AssetImporter extends Importer
{
    protected static ?string $model = Asset::class;

    public static function getColumns(): array
    {
        return [
            ImportColumn::make('owner')->example('Alex')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('phone_number')->example('1234567890')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('establishment_date')->example('2024-05-16')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('floor_number')->example('2')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('assets_location')->example('place')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('asset_location_type')->example('shared_desk/cabin')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('asset_categories')->example('computer/furniture/office_equipment/plant_and_machinery')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('asset_name')->example('asset name')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('date_of_purchase')->example('2024-05-16')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('value_of_purchase')->example('0.00')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('purchase_type')->example('shifted/purchased')
                ->rules(['max:255'])->requiredMapping(),
            ImportColumn::make('asset_condition')->example('working/not_working/dismantled')
                ->rules(['max:255'])->requiredMapping(),
        ];
    }

    public function resolveRecord(): ?Asset
    {
        // return Asset::firstOrNew([
        //     // Update existing records, matching them by `$this->data['column_name']`
        //     'email' => $this->data['email'],
        // ]);

        return new Asset();
    }

    public static function getCompletedNotificationBody(Import $import): string
    {
        $body = 'Your asset import has completed and ' . number_format($import->successful_rows) . ' ' . str('row')->plural($import->successful_rows) . ' imported.';

        if ($failedRowsCount = $import->getFailedRowsCount()) {
            $body .= ' ' . number_format($failedRowsCount) . ' ' . str('row')->plural($failedRowsCount) . ' failed to import.';
        }

        return $body;
    }
}
