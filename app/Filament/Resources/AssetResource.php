<?php

namespace App\Filament\Resources;

use App\Filament\Imports\AssetImporter;
use Filament\Forms;
use Filament\Tables;
use App\Models\Asset;
use Filament\Forms\Get;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Tables\Actions\Action;
use Filament\Tables\Filters\Filter;
use Illuminate\Contracts\View\View;
use App\Filament\Exports\AssetExporter;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Model;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\DatePicker;
use Filament\Tables\Actions\CreateAction;
use Filament\Tables\Actions\ExportAction;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Filament\Resources\AssetResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use pxlrbt\FilamentExcel\Actions\Tables\ExportBulkAction;
use App\Filament\Resources\AssetResource\RelationManagers;
use Webbingbrasil\FilamentAdvancedFilter\Filters\DateFilter;
use Webbingbrasil\FilamentAdvancedFilter\Filters\TextFilter;
use Webbingbrasil\FilamentAdvancedFilter\Filters\NumberFilter;
use Webbingbrasil\FilamentAdvancedFilter\Filters\BooleanFilter;
use Filament\Tables\Actions\ImportAction;
use Illuminate\Support\Facades\Log;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Blade;

class AssetResource extends Resource
{
    protected static ?string $model = Asset::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('owner')
                    ->placeholder('Enter Owner Name')
                    ->required(),
                Forms\Components\TextInput::make('phone_number')
                    ->placeholder('Enter Phone Number')
                    ->required(),
                Forms\Components\DatePicker::make('establishment_date')->required(),
                Forms\Components\TextInput::make('floor_number')->placeholder('Enter Floor Number')->required(),
                Forms\Components\TextInput::make('assets_location')->placeholder('Enter Asset Location')->required(),
                Forms\Components\Select::make('asset_location_type')
                    ->options([
                        'shared_desk' => 'Shared Desk',
                        'cabin' => 'Cabin',
                    ])->required(),
                Forms\Components\Select::make('asset_categories')
                    ->options([
                        'computer' => 'Computer',
                        'furniture' => 'Furniture',
                        'office_equipment' => 'Office Equipment',
                        'plant_and_machinery' => 'Plant & Machinery',
                        'server' => 'Server',
                    ])->live()->required(),
                Forms\Components\Select::make('asset_name')
                    ->options(fn (Get $get): array => match ($get('asset_categories')) {
                        'computer' => array(
                            'cpu' => 'CPU',
                            'monitor' => 'Monitor',
                            'computer' => 'Computer',
                            'laptop' => 'Laptop',
                            'ups' => 'UPS',
                            'thin_client' => 'Thin Client',
                            'printers' => 'Printers',
                            'peripharels' => 'Peripharels',
                        ),
                        'furniture' => array(
                            'table' => 'Table',
                            'chairs' => 'Chairs',
                            'racks' => 'Racks',
                        ),
                        'office_equipment' => array(
                            'electice_installation' => 'Electice Installation',
                            'projector' => 'Projector',
                            'mobile' => 'Mobile',
                            'tv' => 'TV',
                            'water_cooler' => 'Water Cooler',
                            'wifi' => 'WIFI',
                            'ac' => 'AC'
                        ),
                        'plant_and_machinery' => array(
                            'generator' => 'Generator',
                            'ac' => 'AC',
                            'epabx' => 'EPABX',
                            'lift' => 'Lift',
                            'thumb_impression' => 'Thumb Impression',
                            'mplit' => 'MPLIT',
                        ),
                        'server' => array(
                            'server' => 'Server'
                        ),
                        default => [],
                    })->live()->required(),

                Forms\Components\DatePicker::make('date_of_purchase')->required(),
                Forms\Components\TextInput::make('value_of_purchase')->placeholder('Enter Value Of Purchase')->required()->numeric(),
                Forms\Components\Select::make('purchase_type')
                    ->options([
                        'shifted' => 'Shifted',
                        'purchase' => 'Purchase',
                    ])->required(),
                Forms\Components\Select::make('asset_condition')
                    ->options([
                        'working' => 'Working',
                        'dismantled' => 'Dismantled',
                        'not_working' => 'Not Working',
                    ])->required(),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: true),
                TextColumn::make('owner')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('phone_number')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('establishment_date')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('floor_number')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('assets_location')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('asset_location_type')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('asset_categories')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('asset_name')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('date_of_purchase')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('value_of_purchase')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('purchase_type')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
                TextColumn::make('asset_condition')->sortable()->searchable()->toggleable(isToggledHiddenByDefault: false),
            ])
            ->filters([
                // TextFilter::make('owner'),
                // TextFilter::make('assets_location'),
                SelectFilter::make('asset_location_type')->options([
                    'shared_desk' => 'Shared Desk',
                    'cabin' => 'cabin',
                ]),
                SelectFilter::make('asset_categories')->options([
                    'computer' => 'Computer',
                    'furniture' => 'Furniture',
                    'office_equipment' => 'Office Equipment',
                    'plant_and_machinery' => 'Plant & Machinery',
                ]),
                SelectFilter::make('purchase_type')->options([
                    'shifted' => 'Shifted',
                    'purchased' => 'Purchased',
                ]),
                SelectFilter::make('asset_condition')->options([
                    'working' => 'Working',
                    'dismantled' => 'Dismantled',
                    'not_working' => 'Not Working',
                ]),
                // DateFilter::make('created_at'),
                Filter::make('created_at')
                    ->form([
                        Forms\Components\DatePicker::make('created_from'),
                        Forms\Components\DatePicker::make('created_until'),
                    ])
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['created_from'],
                                fn (Builder $query, $date): Builder => $query->whereDate('created_at', '>=', $date),
                            )
                            ->when(
                                $data['created_until'],
                                fn (Builder $query, $date): Builder => $query->whereDate('created_at', '<=', $date),
                            );
                    })
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),

                Action::make('QR')
                    ->label('QR')
                    ->action(fn (Model $record) => $record->advance())
                    ->modalContent(fn (Model $record): View => view(
                        'qr-generate',
                        ['record' => $record],
                    ))
                    ->modalSubmitAction(false)
                    ->modalCancelAction(false),


            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    ExportBulkAction::make(),
                    Tables\Actions\BulkAction::make('bulk-qr')
                        ->label('Bulk QR')
                        ->modalContent(fn (Collection $records): View => view(
                            'bulk-qr-generate',
                            ['records' => $records],
                        ))
                        ->modalSubmitAction(false)
                        ->modalCancelAction(false),
                    Tables\Actions\BulkAction::make('bulk-qr-pdf')
                        ->label('Bulk QR PDF')
                        ->openUrlInNewTab()
                        ->deselectRecordsAfterCompletion()
                        ->action(function (Collection $records) {
                            return response()->streamDownload(function () use ($records) {
                                echo Pdf::loadHTML(
                                    Blade::render('bulk-qr-pdf-generate', ['records' => $records])
                                )->stream();
                            }, 'qr-generated.pdf');
                        }),

                ]),
            ])->headerActions([
                ExportBulkAction::make(),
                ImportAction::make()->importer(AssetImporter::class)

            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAssets::route('/'),
            'create' => Pages\CreateAsset::route('/create'),
            'edit' => Pages\EditAsset::route('/{record}/edit'),
        ];
    }

    public function getAssetById($id)
    {
        return $asset = Asset::findOrFail($id);
    }
}
