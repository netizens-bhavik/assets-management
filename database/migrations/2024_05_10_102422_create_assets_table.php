<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('owner')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('establishment_date')->nullable();
            $table->string('floor_number')->nullable();
            $table->string('assets_location')->nullable();
            $table->string('asset_location_type')->nullable();
            $table->string('asset_categories')->nullable();
            $table->string('asset_name')->nullable();
            $table->string('date_of_purchase')->nullable();
            $table->string('value_of_purchase')->nullable();
            $table->string('purchase_type')->nullable();
            $table->string('asset_condition')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('assets');
    }
};
