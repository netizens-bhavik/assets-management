<style>
    .card {
        /* Add shadows to create the "card" effect */
        /* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); */
        width: 280px;
        margin: 0 auto;
    }

    /* Add some padding inside the card container */
    .card .container {
        padding: 2px 16px;
        margin: auto;
    }
</style>

<div class="card inline-block text-center">
    <div class="container inline-block w-max flex flex-col items-center gap-y-4 text-center">
        {{ QrCode::size(130)->margin(1)->generate($record->id) }}
        <span><b>{{ $record->owner }}</b></span>
    </div>
</div>
