<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Codes</title>
    <style>
        .card {
            /* Add shadows to create the "card" effect */
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            display: inline-block;
            width: 30%;
            text-align: center;
            margin-bottom: 20px
        }

        /* On mouse-over, add a deeper shadow */
        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

        /* Add some padding inside the card container */
        .card .container {
            padding: 2px 16px;
            /* display: inline-block; */
            width: max-content;
            margin: auto;
            /* vertical-align: top; */
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: flex-start;
            row-gap: 10px;
        }

        .card .container span {
            width: 100%;
            display: block;
            text-align: center;
        }
    </style>
</head>


<body>


    @foreach ($records as $record)
        <div class="card">
            <div class="container" style="text-align: center">
                <img src="data:image/png;base64, {!! base64_encode(
                    QrCode::size(128)->margin(1)->generate($record->id),
                ) !!}" alt="QR Code">
                <span ><b>{{ $record->owner }}</b></span>
            </div>
        </div>
    @endforeach


</body>

</html>
